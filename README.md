# ELK-learning-analytics
Python scripts in the Jupyter notebook to help to get quick knowledge about data from KYPO Training or clean them up. 

## Requirements and Prerequisities

Installed conda ([Conda installation tutorial](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html)) in version 4.10.3 (higher should works too)

Running elastic from [ELK Portal Command Events Repository](https://gitlab.fi.muni.cz/cybersec/elk-portal-commands-events) and completed the data import (described in the repository with elastic search as well). The example configuration of the script is made for data `locust-3302-may-2020 (Echo2019 - Kobylka 3302/2020-05-06 PA197 seminar)` and `knowledge-base-october-2021`.

## Inicialization

In the repository folder, run `conda env create --file environment.yml` to create the environment and install all modules and dependencies.

After successful environment initialization, run `conda activate kypo_analytics`. To check the correct environment is active, you can run the `conda env list` and on line with `kypo_analytics` should be visible asterisk.

In the environment, run command `jupyter-notebook` which opens `http://localhost:8888` in the broswer and there in files you should select `elk_scripts.ipynb` file.

If you are not familiar with Jupyter notebooks, here is a tutorial and explanation `https://www.datacamp.com/community/tutorials/tutorial-jupyter-notebook`. You can also start simply by clicking on the cell you want to run, and on the top of the page, select Cell > Run Cells (and do not forget that if you're going to use any function or variable from another cell, you should run it first to load it to memory).