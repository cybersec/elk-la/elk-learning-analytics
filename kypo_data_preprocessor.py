""" KYPO Data Preprocessor
This module provides a class KYPODataPreprocessor which
extends the Python Elasticsearch client for automated query builders,
filters and datatransformation to Pandas DataFrames.
"""

import re
import pandas as pd
import numpy as np
from elasticsearch import Elasticsearch


class KYPODataPreprocessor:

    """
    This class is an abstracted Elasticsearch connection for
    fetching Training events and Commands with transformation
    to Pandas DataFrames.
    """

    def __init__(self, host, port):
        self.es = Elasticsearch(HOST=host, PORT=port)
        self.sandbox_ids = []
        self.time_from = None
        self.time_to = None

    def set_sandbox_ids(self, sb_id):
        """ Setter for sandbox IDs """

        self.sandbox_ids = sb_id

    def set_time_from(self, time_from):
        """ Setter for date and time from """

        self.time_from = time_from

    def set_time_to(self, time_to):
        """ Setter for date and time to """

        self.time_to = time_to

    def fetch_bash_logs(self):
        """
        Fetch training commands based on attributes
        settings and transform data to Pandas DataFrame.
        """

        path_bash = ','.join(list(map(lambda x: \
                             'kypo.logs.console.bash.command.pool=*.sandbox=' \
                             + x, self.sandbox_ids)))
        resp_bash = self.elasticsearch_search('bash logs', path_bash,
                self.build_date_range_query('timestamp_str'))
        return transform_bash_logs_to_df(resp_bash)

    def fetch_bash_logs_raw(self):
        """
        This function builds a request and fetch raw data from the Elasticsearch
        """

        path_bash = ','.join(list(map(lambda x: \
                             'kypo.logs.console.bash.command.pool=*.sandbox=' \
                             + x, self.sandbox_ids)))
        resp_bash = self.elasticsearch_search('bash logs', path_bash,
                self.build_date_range_query('timestamp_str'))
        return resp_bash

    def explore_bash_logs(self):
        """
        Explore all training commands in the Elasticsearch and gives the information
        about data.
        """

        path_bash = 'kypo.logs.console.bash.command.pool=*.sandbox=*'
        resp_bash = self.elasticsearch_search('bash logs', path_bash,
                {'size': 9999})
        bash_df = transform_bash_logs_to_df(resp_bash)
        return bash_df.groupby(['sandbox_id'
                               ]).agg({'timestamp': [np.min, np.max],
                'cmd_raw': ['count']})

    def explore_events(self):
        """
        Explore all training events in the Elasticsearch and gives the information
        about data.
        """

        path_events = \
            'kypo.*_evt.*.sandbox=*.definition=*.instance=*.run=*'
        resp_events = self.elasticsearch_search('events', path_events,
                {'size': 9999})
        events_df = transform_events_to_df(resp_events)
        return events_df.groupby(['sandbox_id'
                                 ]).agg({'timestamp': [np.min, np.max],
                'event_type': ['count']})

    def fetch_events(self):
        """
        Fetch training events based on attributes
        settings and transform data to Pandas DataFrame.
        """

        path_events = ','.join(list(map(lambda x: \
                               'kypo.*_evt.*.sandbox=' + x \
                               + '.definition=*.instance=*.run=*',
                               self.sandbox_ids)))
        resp_events = self.elasticsearch_search('events', path_events,
                self.build_date_range_query('syslog.@timestamp'))
        return transform_events_to_df(resp_events)

    def elasticsearch_search(
        self,
        call_name,
        index,
        body,
        ):
        """
        Search data in elastic and returns the response.
        """

        resp = self.es.search(index=index, body=body, ignore=[404])
        if 'error' in resp:
            print ('[!] Request problem in', call_name, '-',
                   resolve_elastic_error(resp['error']['type']))
            return []
        return resp['hits']['hits']

    def build_date_range_query(self, timestamp_field):
        """
        Build a query with filter based on attributes time_from and
        time_to.
        """

        # https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-range-query.html

        return {'size': 9999,
                'query': {'range': {timestamp_field: {'gte': self.time_from \
                + '+02:00', 'lte': self.time_to + '+02:00'}}}}


def transform_bash_logs_to_df(bash_logs):
    """
    This function transforms raw training commands data from
    Elasticsearch to Pandas DataFrame.
    """

    cols = {
        'sandbox_id': [],
        'timestamp': [],
        'cmd_raw': [],
        'cmd_type': [],
        }
    for log in bash_logs:
        if 'cmd' in log['_source']:
            cols['sandbox_id'].append(re.search(r'sandbox=(\d*)',
                                    log['_index']).group(1))
            cols['timestamp'].append(log['_source']['timestamp_str'])
            cols['cmd_raw'].append(log['_source']['cmd'])
            cols['cmd_type'].append(log['_source']['cmd_type'])

    df = pd.DataFrame(cols)
    df['sandbox_id'] = df['sandbox_id'].astype('int')
    df['timestamp'] = df['timestamp'].apply(pd.to_datetime)
    df['cmd_type'] = df['cmd_type'].astype('category')
    df['cmd_raw'] = df['cmd_raw'].astype('string')
    return df


def transform_events_to_df(events):
    """
    This function transforms raw training events data from
    Elasticsearch to Pandas DataFrame.
    """

    # print(events)

    cols = {
        'sandbox_id': [],
        'timestamp': [],
        'event_type': [],
        'level': [],
        }
    for event in events:
        if 'message' not in event['_source']:
            cols['sandbox_id'].append(event['_source']['sandbox_id'])
            cols['timestamp'].append(event['_source']['syslog'
                    ]['@timestamp'])
            cols['event_type'].append(event['_source']['type'].split('.'
                    )[-1])
            cols['level'].append((event['_source']['level'] if 'level'
                                    in event['_source'
                                    ] else event['_source']['phase_id']))

    df = pd.DataFrame(cols)
    df['sandbox_id'] = df['sandbox_id'].astype('int')
    df['timestamp'] = df['timestamp'].apply(pd.to_datetime)
    df['event_type'] = df['event_type'].astype('category')
    df['level'] = df['level'].astype('int')
    return df


def resolve_elastic_error(error):
    """
    This function tries to parse known errors and prints them to the
    console with a more informative description.
    """

    error_switch = \
        {'index_not_found_exception': 'Sandbox given on input does not exist'}
    if error in error_switch:
        return error_switch[error]
    return error
