"""
This module consists of utility functions for KYPO training data.
"""
import re
import numpy as np
import pandas as pd


def parse_cmd(cmd_raw):
    """
    This function parse raw command from command line, disregard sudo,
    sort the parameteres in alphabetic order and return it as tupple (cmd, opts)
    """

    opts = cmd_raw.split()
    cmd = (opts[1] if opts[0] == 'sudo' and len(opts) > 1 else opts[0])
    opts = ' '.join((opts[1:] if opts[0] != 'sudo' else opts[2:]))
    opts = list(map(lambda x: x.group(),
                re.finditer(r'(".*")|(--?\w+\s[^\s-]*)|([^\s-]+)',
                opts)))
    opts.sort()
    return (cmd, opts)


def format_delta_time(sec_delta):
    """
    Formating time intarval to hh:mm:ss
    """

    ts = sec_delta.total_seconds()
    (hours, remainder) = divmod(ts, 3600)
    (minutes, seconds) = divmod(remainder, 60)
    return '{}:{:02d}:{:02d}'.format(int(hours), int(minutes),
            int(seconds))


def filter_logs_in_level_range(
    logs_df,
    events_df,
    level_from,
    level_to,
    ):
    """
    For given training commands filter all with timestamp between two levels.
    """

    dfs = []
    levels = events_df.groupby(['sandbox_id', 'level'
                               ]).agg({'timestamp': [np.min, np.max]})
    sids = set(map(lambda x: x[0], list(levels.index.values)))
    for sid in sids:
        dfs.append(logs_df.loc[(logs_df.sandbox_id == sid)
                   & (logs_df.timestamp >= levels.loc[(sid,
                   level_from)].timestamp.amin) & (logs_df.timestamp
                   <= levels.loc[(sid, level_to)].timestamp.amax)])
    return pd.concat(dfs)
